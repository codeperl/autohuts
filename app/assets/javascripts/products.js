// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$('.products.index').ready(function() {
    $(".product-items1").stalactite();
});

$('.products.new, .products.edit').ready(function() {
    $('#product_tag_list').select2({theme: "bootstrap"});
});