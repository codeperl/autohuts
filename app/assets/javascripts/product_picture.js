// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function previewUploadedImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        /*var imageType = /image.*!/;
        if (!file.type.match(imageType)) {
            continue;
        }*/
        var img=document.getElementById("preview-product-picture");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function(aImg) {
            return function(e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
}
