class RoleNameValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    unless value == Roles::CONSUMER || value == Roles::DEALER
      record.errors[attribute] << (options[:message] || "has error.")
    end

  end
end