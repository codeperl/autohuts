class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @product = Product.find(params[:comment][:commentable_id])
    @comment = @product.comment_threads.build(permitted_params)
    authorize @comment

    if (current_user.has_role? Roles::DEALER) || (current_user.has_role? Roles::CONSUMER)
      @comment.user = current_user
      if @comment.save
        redirect_to product_path(@product), flash: {success: "Comment had been saved successfully!" }
      else
        redirect_to product_path(@product), flash: {alert: "Your comment has not been saved!" }
      end
    end
  end

  protected
    def permitted_params
      params.require(:comment).permit(:subject, :body, :commentable_id, :commentable_type)
    end
end
