class IndexController < ApplicationController
  def index
    @shops = Shop.find_featured.shuffle
    @products = Product.find_featured.shuffle
    @recent_shops = Shop.recent
    @recent_products = Product.recent
  end
end