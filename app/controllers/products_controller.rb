class ProductsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show, :role_products, :products_from_individuals]

  def index
    if (not params.blank?) && (params[:category].present?)
      category_id = params[:category]
      unless category_id.blank?
        @categories = Category.find_categories_by_category_id(category_id)
        @products = Product.where(category: @categories).order(created_at: :DESC).page params[:page]
        @category = Category.find(category_id)
      end
    else
      @products = Product.lists.page params[:page]
    end
  end

  def new
    @product = Product.new
    authorize @product
    if current_user.has_role? Roles::DEALER
      @shop = current_user.profile.specific.shop
    else
      @consumerProfile = current_user.profile.specific
    end
    @product.product_specifications.build
    @product.product_pictures.build
    @product.product_attachments.build
  end

  def create
    @product = Product.new(permitted_params)
    authorize @product

    category = Category.find_by(id: params[:product][:category_id])
    unless category.blank?
      @product.category = category
    end
    # @product.tag_list.add(params[:product][:tag_list], parse: true)

    if current_user.has_role? Roles::DEALER
      shop = current_user.profile.specific.shop
      shop_product = ShopProduct.new
      shop_product.shop = shop
      @product.shop_product = shop_product

      if @product.save
        redirect_to @product, flash: {success: "Product had been created successfully!" }
      else
        render :new
      end
    elsif current_user.has_role? Roles::CONSUMER
      consumer_profile = current_user.profile.specific
      consumer_profile_product = ConsumerProfileProduct.new
      consumer_profile_product.consumer_profile = consumer_profile
      @product.consumer_profile_product = consumer_profile_product

      if @product.save
        redirect_to @product, flash: {success: "Product had been created successfully!" }
      else
        render :new
      end
    end
  end

  def edit
    @product = Product.find(params[:id])
    authorize @product
  end

  def update
    @product = Product.find(params[:id])
    authorize @product

    category = Category.find_by(id: params[:product][:category_id])
    unless category.blank?
      @product.category = category
    end
    # @product.tag_list.add(params[:product][:tag_list], parse: true)

    if (current_user.has_role? Roles::DEALER)
      if @product.update(permitted_params)
        @shop = current_user.profile.specific.shop
        redirect_to @product, flash: {success: "Product had been updated successfully!" }
      else
        render :edit
      end
    elsif (current_user.has_role? Roles::CONSUMER)
      if @product.update(permitted_params)
        @consumerProfile = current_user.profile.specific
        redirect_to @product, flash: {success: "Product had been updated successfully!" }
      else
        render :edit
      end
    end
  end

  def show
    @product = Product.find_one_by_id(params[:id])
  end

  def destroy
  end

  def role_products
    if (not params.blank?) && (params[:product_type].present?)
      if params[:product_type] == ProductTypes::SHOP
        @products = Product.lists.shop_products.page params[:page]
      elsif params[:product_type] == ProductTypes::INDIVIDUAL
        @products = Product.lists.products_from_individuals.page params[:page]
      end

      category_id = params[:category]
      unless category_id.blank?
        @categories = Category.find_categories_by_category_id(category_id)
        @products = @products.where(category: @categories).page params[:page]
        @category = Category.find(category_id)
      end
    end
  end

  def products_from_individuals
    @products = Product.lists.products_from_individuals.page params[:page]
  end

  def user_products
    if( (current_user.has_role? Roles::DEALER) )
      unless current_user.profile.specific.shop.blank?
        @products_containers = current_user.profile.specific.shop.shop_products
      else
        @products_containers = nil
      end
    elsif ( current_user.has_role? Roles::CONSUMER )
      @products_containers = current_user.profile.specific.consumer_profile_products
    end
  end

  private
    def permitted_params
      params.require(:product).permit(:name, :description, :price_including_vat, :vat_percent, :stock, :category, product_specifications_attributes: [:id, :name, :value, :_destroy], product_pictures_attributes: [:id, :caption, :picture, :cover_picture, :_destroy], product_attachments_attributes: [:id, :name, :document, :_destroy])
    end
end
