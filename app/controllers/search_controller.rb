class SearchController < ApplicationController

  def index
    @search_term = SearchTerm.new
  end

  def result
    unless params.blank? or (not defined? params[:search_term][:term])
      @search_term = SearchTerm.new(search_term_permitted_params)
      @pg_search_documents = PgSearch.multisearch(params[:search_term][:term])
    else
      @search_term = SearchTerm.new
      render :result
    end
  end

  protected
    def search_term_permitted_params
      params.require(:search_term).permit(:term)
    end
end