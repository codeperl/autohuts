class ProductPicturesController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :show_large]

  def index
    @products = ProductPicture.all
  end

  def new
    @product_picture = ProductPicture.new
    authorize @product_picture
  end

  def create
    @product_picture = ProductPicture.new(permitted_params)
    authorize @product_picture
    if current_user.has_role? Roles::DEALER
      if @product_picture.save
        redirect_to @product_picture
      else
        render :new
      end
    end
  end

  def edit
    @product_picture = ProductPicture.find(params[:id])
    authorize @product_picture
  end

  def update
    @product_picture = ProductPicture.find(params[:id])
    authorize @product_picture
    if current_user.has_role? Roles::DEALER
      if @product_picture.update(permitted_params)
        redirect_to @product_picture
      else
        render :edit
      end
    end
  end

  def show
    @product_picture = ProductPicture.find(params[:id])
  end

  def destroy
  end

  def show_large
    @product_picture = ProductPicture.find(params[:id])
    render :layout => false
  end

  private
    def permitted_params
      params.require(:product_picture).permit(:caption, :picture)
    end
end
