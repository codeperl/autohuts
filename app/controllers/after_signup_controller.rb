class AfterSignupController < ApplicationController

  include Wicked::Wizard

  before_filter :authenticate_user!

  steps :update_profile, :create_shop


  def show
    @user = current_user
    case step
      when :update_profile
        @profile = @user.profile.specific

        if current_user.has_role? Roles::DEALER
          @business_profile = @profile
        elsif current_user.has_role? Roles::CONSUMER
          @consumer_profile = @profile
        end

      when :create_shop
        if current_user.has_role? Roles::DEALER
          @shop = Shop.new
        elsif current_user.has_role? Roles::CONSUMER
          redirect_to root_url , flash: {success: "Signed up successfully!" }
          return
        end
    end

    render_wizard
  end

  def update
    @user = current_user
    @profile = @user.profile.specific

    if params.has_key?("business_profile")
      @business_profile = BusinessProfile.find(@profile.id)
      if current_user.has_role? Roles::DEALER
        render_wizard @business_profile
      end
    elsif params.has_key?("consumer_profile")
      @consumer_profile = ConsumerProfile.find(@profile.id)
      if current_user.has_role? Roles::CONSUMER
        render_wizard @consumer_profile
      end
    elsif params.has_key?("shop")
      if current_user.has_role? Roles::DEALER
        @shop = Shop.new(shop_permitted_params)
        @shop.business_profile = @profile
        render_wizard @shop
      else
        render_wizard
      end
    end
  end


  private

    def shop_permitted_params
      params.require(:shop).permit(:name, :sub_domain, :description, :featured, :address, :mobile, :cover_photo, :picture, :national_id, :terms_conditions, :picture, shop_attachments_attributes: [:id, :name, :document, :_destroy])
    end
end
