class BusinessProfilesController < ApplicationController

  before_filter :authenticate_user!, except: [:public_show]

  def edit
    @business_profile = BusinessProfile.find(params[:id])
    authorize @business_profile
  end

  def update
    @business_profile = BusinessProfile.find(params[:id])
    authorize @business_profile
    if current_user.has_role? Roles::DEALER
      if @business_profile.update(permitted_params)
        redirect_to @business_profile, flash: {success: "Profile had been updated successfully!" }
      else
        render :edit
      end
    end
  end

  def show
    @business_profile = BusinessProfile.find(params[:id])
    authorize @business_profile
  end

  def public_show
    @business_profile = BusinessProfile.find(params[:id])
  end

  protected
    def permitted_params
      params.require(:business_profile).permit(:contact_person, :helpline, :website, :address, :phone, :mobile, business_profile_attachments_attributes: [:id, :name, :document, :_destroy])
    end

end
