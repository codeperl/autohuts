class ShopsController < ApplicationController

  before_filter :authenticate_user!, except: [:index, :show]

  def index
    @shops = Shop.all.order(created_at: :DESC).page params[:page]
  end

  def new
    @shop = Shop.new
    authorize @shop
    @shop.shop_attachments.build
  end

  def create
    @shop = Shop.new(permitted_params)
    if current_user.has_role? Roles::DEALER
      @shop.business_profile = current_user.profile.specific
    end
    authorize @shop
    @shop.business_type_list.add(params[:shop][:business_type_list], parse: true)

    if @shop.save
      redirect_to @shop, flash: {success: "Shop had been created successfully!" }
    else
      render :new
    end
  end

  def edit
    @shop = Shop.find(params[:id])
    authorize @shop
  end

  def update
    @shop = Shop.find(params[:id])
    authorize @shop

    if current_user.has_role? Roles::DEALER
      @shop.business_types = []
      @shop.business_type_list.add(params[:shop][:business_type_list], parse: true)
      if @shop.update(permitted_params)
        redirect_to @shop, flash: {success: "Shop had been updated successfully!" }
      else
        render :edit
      end
    end
  end

  def show
    @shop = Shop.find(params[:id])
    @shop_products = @shop.shop_products
  end

  def destroy
  end

  private
    def permitted_params
      params.require(:shop).permit(:name, :sub_domain, :description, :address, :business_type_list, :mobile, :cover_photo, :picture, :national_id, :trade_license, :terms_conditions, :picture, shop_attachments_attributes: [:id, :name, :document, :_destroy])
    end
end
