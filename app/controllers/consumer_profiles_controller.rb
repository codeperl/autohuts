class ConsumerProfilesController < ApplicationController

  before_filter :authenticate_user!, except: [:public_show]

  def edit
    @consumer_profile = ConsumerProfile.find(params[:id])
    authorize @consumer_profile
  end

  def update
    @consumer_profile = ConsumerProfile.find(params[:id])
    authorize @consumer_profile
    if current_user.has_role? Roles::CONSUMER
      if @consumer_profile.update(permitted_params)
        redirect_to @consumer_profile
      else
        render :edit
      end
    end
  end

  def show
    @consumer_profile = ConsumerProfile.find(params[:id])
    authorize @consumer_profile
  end

  def public_show
    @consumer_profile = ConsumerProfile.find(params[:id])
  end

  protected
    def permitted_params
      params.require(:consumer_profile).permit(:name, :address, :phone, :mobile)
    end
end