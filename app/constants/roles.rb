class Roles
  # Values for dropdown
  SUPER_ADMIN = "superadmin"
  ADMIN = "admin"
  DEALER = "dealer"
  CONSUMER = "consumer"

  # Textual representation at dropdown
  CONSUMER_TEXT = 'Consumer'
  DEALER_TEXT = 'Dealer'
end