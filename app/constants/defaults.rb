class Defaults
  NO_IMAGE = 'no_image.jpg'
  NO_COVER_PHOTO = 'no_cover_photo.png'
  NO_LOGO = 'no_logo.png'
  PRODUCT_ADVERTISEMENT_PLACEHOLDER = 'productadplaceholder.png'
  SHOP_ADVERTISEMENT_PLACEHOLDER = 'shopadplaceholder.png'
  FIND_US_AT_FACEBOOK = 'find_us_at_facebook.png'
  PRODUCT_NAME_LENGTH_TILL = 22
  PRODUCT_DESCRIPTION_LENGTH_TILL = 40
  SHOP_NAME_LENGTH_TILL = 27
  SHOP_DESCRIPTION_LENGTH_TILL = 40
end