module BusinessProfilesHelper
  def show_business_profile(business_profile)
    render :partial => 'partials/business_profiles/view', :locals => {business_profile: business_profile}
  end
end