module ApplicationHelper

  def bootstrap_class_for flash_type
    {success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info"}[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do
               concat content_tag(:button, 'x', class: "close", data: {dismiss: 'alert'})
               concat message
             end)
    end

    nil
  end

  def user_menus(user_signed_in, user)

    if user_signed_in
      render :partial => 'partials/navigations/authenticated_nav', :locals => {user: user}
    else
      render :partial => 'partials/navigations/anonymous_nav'
    end

  end

  def menu_by_user_role(user)
    if user.has_role? Roles::DEALER
      render :partial => 'partials/navigations/business_nav', :locals => {user: user}
    elsif user.has_role? Roles::CONSUMER
      render :partial => 'partials/navigations/consumer_nav', :locals => {user: user}
    end
  end

  def shop_by_user_role(user)
    if user.profile.specific.shop.nil?
      render :partial => 'partials/navigations/new_shop', :locals => {user: user}
    elsif user.profile.specific.shop.id
      render :partial => 'partials/navigations/manage_shop', :locals => {user: user}
    end
  end

  def product_by_user_role(user)
    unless user.profile.specific.shop.blank?
      render :partial => 'partials/navigations/product_menu', :locals => {user: user}
    end
  end

  def tags_navigation
    @tags = Product.get_tags
    unless @tags.blank?
      render :partial => 'partials/navigations/tags', :locals => {tags: @tags}
    end
  end

  ############################### AWESOME NESTED SET MENU #########################################
  def nested_li(objects, &block)
    objects = objects.order(:lft) if objects.is_a? Class

    return '' if objects.size == 0

    all_products_url = render :partial => 'partials/navigations/all_products_url', :locals => {products_path: products_path}
    products_by_categories_title = render :partial => 'partials/navigations/sorted_by_title'
    output = <<NAV
    <ul class="nav navbar-nav">
NAV

    output << products_by_categories_title
    output << all_products_url

    output << '<li class="dropdown">'

    path = [nil]

    objects.each_with_index do |o, i|
      if o.parent_id != path.last
        # We are on a new level, did we descend or ascend?
        if path.include?(o.parent_id)
          # Remove the wrong trailing path elements
          while path.last != o.parent_id
            path.pop
            output << '</li></ul>'
          end
          output << '</li><li class="dropdown">'
        else
          path << o.parent_id
          output << '<ul class="dropdown-menu" role="menu"><li>'
        end
      elsif i != 0
        output << '</li><li>'
      end
      category_url = render :partial => 'partials/navigations/category_url', :locals => {object: o}
      output << category_url
    end

    output << "</li></ul>" * path.length
    output.html_safe
  end

  def sorted_nested_li(objects, order, &block)
    nested_li sort_list(objects, order), &block
  end

  private

  def sort_list(objects, order)
    objects = objects.order(:lft) if objects.is_a? Class

    # Partition the results
    children_of = {}
    objects.each do |o|
      children_of[ o.parent_id ] ||= []
      children_of[ o.parent_id ] << o
    end

    # Sort each sub-list individually
    children_of.each_value do |children|
      children.sort_by! &order
    end

    # Re-join them into a single list
    results = []
    recombine_lists(results, children_of, nil)

    results
  end

  def recombine_lists(results, children_of, parent_id)
    if children_of[parent_id]
      children_of[parent_id].each do |o|
        results << o
        recombine_lists(results, children_of, o.id)
      end
    end
  end
end