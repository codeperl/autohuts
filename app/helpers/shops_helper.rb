module ShopsHelper
  def show_contact_information(shop)
    render :partial => 'partials/shops/contact', :locals => {shop: shop}
  end
end