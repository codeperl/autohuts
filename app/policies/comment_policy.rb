class CommentPolicy < ApplicationPolicy
  attr_accessor :user, :comment

  def initialize(user, comment)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @comment = comment
  end

  def create?
    # If user has business or consumer role he/she can create a comment.
    (@user.has_role? Roles::DEALER) || (@user.has_role? Roles::CONSUMER)
  end

end