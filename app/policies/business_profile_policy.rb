class BusinessProfilePolicy < ApplicationPolicy
  attr_accessor :user, :business_profile

  def initialize(user, business_profile)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @business_profile = business_profile
  end

  def edit?
    # if user has business role and user has business profile same as current business profile then
    # he/she can edit, update.
    (@user.has_role? Roles::DEALER) && (@user.profile.specific == @business_profile)
  end

  def update?
    # if user has business role and user has business profile same as current business profile then
    # he/she can edit, update.
    (@user.has_role? Roles::DEALER) && (@user.profile.specific == @business_profile)
  end

  def show?
    true
  end
end