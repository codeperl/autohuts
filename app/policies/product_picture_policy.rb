class ProductPicturePolicy < ApplicationPolicy
  attr_accessor :user, :product_picture

  def initialize(user, product_picture)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @product_picture = product_picture
  end

  def index?
    # Everyone should be able to see all product pictures.
    true
  end

  def new?
    # If user own the current product then he/she should be able to create a new product picture.
    (@user.has_role? Roles::DEALER) || (@user.has_role? Roles::CONSUMER)
  end

  def create?
    # If user own the current product then he/she should be able to create a new product picture.
    (@user.has_role? Roles::DEALER) || (@user.has_role? Roles::CONSUMER)
  end

  def edit?
    # If user own the current product and own the current product picture then he/she should be able to edit/update it.
    if( (@user.has_role? Roles::DEALER) )
      if(not @product_picture.product.shop_product.blank?)
        if ( @user.profile.specific.shop == @product_picture.product.shop_product.shop )
          return true
        else
          return false
        end
      end
    elsif ( @user.has_role? Roles::CONSUMER )
      if(not @product_picture.product.consumer_profile_product.blank?)
        if ( user.profile.specific == @product_picture.product.consumer_profile_product.consumer_profile )
          return true
        else
          return false
        end
      end
      return false
    end
  end

  def update?
    # If user own the current product and own the current product picture then he/she should be able to edit/update it.
    if( (@user.has_role? Roles::DEALER) )
      if(not @product_picture.product.shop_product.blank?)
        if ( @user.profile.specific.shop == @product_picture.product.shop_product.shop )
          return true
        else
          return false
        end
      end
    elsif ( @user.has_role? Roles::CONSUMER )
      if(not @product_picture.product.consumer_profile_product.blank?)
        if ( user.profile.specific == @product_picture.product.consumer_profile_product.consumer_profile )
          return true
        else
          return false
        end
      end
      return false
    end
  end

  def destroy
    # If user own the current product and own the current product picture then he/she should be able to edit/update it.
    if( (@user.has_role? Roles::DEALER) )
      if(not @product_picture.product.shop_product.blank?)
        if ( @user.profile.specific.shop == @product_picture.product.shop_product.shop )
          return true
        else
          return false
        end
      end
    elsif ( @user.has_role? Roles::CONSUMER )
      if(not @product_picture.product.consumer_profile_product.blank?)
        if ( user.profile.specific == @product_picture.product.consumer_profile_product.consumer_profile )
          return true
        else
          return false
        end
      end
      return false
    end
  end

  def show?
    # Product picture should be viewable to all.
    true
  end

  def show_large?
    # Large product picture should be viewable to all.
    true
  end
end