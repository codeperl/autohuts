class ProductPolicy < ApplicationPolicy
  attr_accessor :user, :product

  def initialize(user, product)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @product = product
  end

  def index?
    # Product should be viewable to all.
    true
  end

  def new?
    # If user own a shop then and only then he/she should be able to create product.
    (@user.has_role? Roles::DEALER) || (@user.has_role? Roles::CONSUMER)
  end

  def create?
    # If user own a shop then and only then he/she should be able to create product.
    (@user.has_role? Roles::DEALER) || (@user.has_role? Roles::CONSUMER)
  end

  def edit?
    # If user own the current product then and only then he/she should be able to edit/update it.
    if( (@user.has_role? Roles::DEALER) )
      if(not @product.shop_product.blank?)
        if ( @user.profile.specific.shop == @product.shop_product.shop )
          return true
        else
          return false
        end
      end
    elsif ( @user.has_role? Roles::CONSUMER )
      if(not @product.consumer_profile_product.blank?)
        if ( user.profile.specific == @product.consumer_profile_product.consumer_profile )
          return true
        else
          return false
        end
      end
      return false
    end
  end

  def update?
    # If user own the current product then and only then he/she should be able to edit/update it.
    if( (@user.has_role? Roles::DEALER) )
      if(not @product.shop_product.blank?)
        if ( @user.profile.specific.shop == @product.shop_product.shop )
          return true
        else
          return false
        end
      end
    elsif ( @user.has_role? Roles::CONSUMER )
      if(not @product.consumer_profile_product.blank?)
        if ( user.profile.specific == @product.consumer_profile_product.consumer_profile )
          return true
        else
          return false
        end
      end
      return false
    end
  end

  def destroy
    # If user own the current product then and only then he/she should be able to destroy it.
    if( (@user.has_role? Roles::DEALER) )
      if(not @product.shop_product.blank?)
        if ( @user.profile.specific.shop == @product.shop_product.shop )
          return true
        else
          return false
        end
      end
    elsif ( @user.has_role? Roles::CONSUMER )
      if(not @product.consumer_profile_product.blank?)
        if ( user.profile.specific == @product.consumer_profile_product.consumer_profile )
          return true
        else
          return false
        end
      end
      return false
    end
  end

  def show?
    # Product should be viewable to all.
    true
  end
end