class BusinessProfileAttachmentPolicy < ApplicationPolicy
  attr_accessor :user, :business_profile_attachment

  def initialize(user, business_profile_attachment)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @business_profile_attachment = business_profile_attachment
  end

  def index?
    # Anyone can see the business profile attachments, because these are publicly viewable documents.
    true
  end

  def new?
    # If user has business profile then and only then he/she can create business profile attachment.
    (@user.has_role? Roles::DEALER)
  end

  def create?
    # If user has business profile then and only then he/she can create business profile attachment.
    (@user.has_role? Roles::DEALER)
  end

  def edit?
    # If user has business profile and if the current business profile attachment has the same business
    # profile then and only then he can edit. update.
    (@user.has_role? Roles::DEALER) && (@user.profile.specific == @business_profile_attachment.profile)
  end

  def update?
    # If user has business profile and if the current business profile attachment has the same business
    # profile then and only then he can edit. update.
    (@user.has_role? Roles::DEALER) && (@user.profile.specific == @business_profile_attachment.profile)
  end

  def show?
    # Anyone can see the business profile attachment details
    true
  end
end