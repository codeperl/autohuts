class ConsumerProfilePolicy < ApplicationPolicy
  attr_accessor :user, :consumer_profile

  def initialize(user, consumer_profile)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @consumer_profile = consumer_profile
  end

  def edit?
    # if user has consumer role and user has consumer profile same as current consumer profile then
    # he/she can edit, update.
    (@user.has_role? Roles::CONSUMER) && (@user.profile.specific == @consumer_profile)
  end

  def update?
    # if user has consumer role and user has consumer profile same as current consumer profile then
    # he/she can edit, update.
    (@user.has_role? Roles::CONSUMER) && (@user.profile.specific == @consumer_profile)
  end

  def show?
    # Everyone should be able to view consumer profile
    true
  end
end