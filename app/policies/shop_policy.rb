class ShopPolicy < ApplicationPolicy
  attr_accessor :user, :shop

  def initialize(user, shop)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user

    @user = user
    @shop = shop
  end

  def index?
    # Shops should be visible by everyone
    true
  end

  def new?
    # If user has business role and user has no shop then he/she should be able to create a new one.
    (@user.has_role? Roles::DEALER) && (user.profile.specific.shop.nil?)
  end

  def create?
    # If user has business role then he/she should be able to create a new one.
    (@user.has_role? Roles::DEALER)
  end

  def edit?
    # If user has business role and user own current shop then he/should be able to edit/update it.
    (@user.has_role? Roles::DEALER) && (user.profile.specific.shop == @shop)
  end

  def update?
    # If user has business role and user own current shop then he/should be able to edit/update it.
    (@user.has_role? Roles::DEALER) && (user.profile.specific.shop == @shop)
  end

  def destroy
    # If user has business role and user own current shop then he/should be able to destroy it.
    (@user.has_role? Roles::DEALER) && (user.profile.specific.shop == @shop)
  end

  def show?
    # Shop should be visible by everyone
    true
  end

  class Scope < Scope
    def resolve
      scope.where(user: user)
    end
  end
end