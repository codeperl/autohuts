# encoding: utf-8

require_relative 'uploader_extension_white_list'

class ProductPictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def sanitize_regexp
    CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/
  end

  # we process images with a custom method (read above)
  # process :resize_to_fit => :default

  # thumb processing, we assume that each model has a "thumb" version
  version :thumb do
    process :dynamic_resize_to_fill => :thumb
  end

  # small processing, we assume that each model has a "small" version
  version :small do
    process :dynamic_resize_to_fill => :small
  end

  # conditional processing: we process "logo" version only if it was defined in model
  #version :logo, :if => :has_logo_size? do
  version :logo do
    process :dynamic_resize_to_fill => :logo
  end

  # a lame wrapper to resize_to_fit method
  def dynamic_resize_to_fill(size)
    resize_to_fill *(model.class::IMAGE_SIZES[size])
  end

  # here's the metaprogramming magic!
  # we check if the called method matches "has_VERSION_size?"
  # VERSION is a version name for image size
  def method_missing(method, *args)
    # we've already defined "has_VERSION_size?", so if a method with
    # similar name is missed, it should return false
    return false if method.to_s.match(/has_(.*)_size\?/)
    super
  end

  include UploaderExtensionWhiteList

  protected
    # the method called at the start
    # it checks for <model>::IMAGE_SIZES hash and define a custom method "has_VERSION_size?"
    # (more on this later in the article)
    def setup_available_sizes(file)
      model.class::IMAGE_SIZES.keys.each do |key|
        self.class_eval do
          define_method("has_#{key}_size?".to_sym) { true }
        end
      end
    end
end
