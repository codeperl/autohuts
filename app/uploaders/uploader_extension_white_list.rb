module UploaderExtensionWhiteList
  def extension_white_list
    %w(jpg jpeg png)
  end
end