ActiveAdmin.register Product do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :name, :description, :price_including_vat, :vat_percent, :stock, :featured, :category_id, product_specifications_attributes: [:id, :name, :value, :_destroy], product_pictures_attributes: [:id, :caption, :picture, :cover_picture, :_destroy], product_attachments_attributes: [:id, :name, :document, :_destroy]

end