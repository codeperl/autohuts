ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  index do
    selectable_column
    id_column
    column :username
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :username
  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "User Details" do

      selected_role = ''

      if f.object.has_role? Roles::CONSUMER
        selected_role = Roles::CONSUMER
      elsif f.object.has_role? Roles::DEALER
        selected_role = Roles::DEALER
      end

      f.input :username
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :role_name, :label => "Role", as: :select, collection: options_for_select([[Roles::CONSUMER_TEXT, Roles::CONSUMER], [Roles::DEALER_TEXT, Roles::DEALER]], selected_role), include_blank: false
    end
    f.actions
  end

  controller do

    def update
      params.permit!
      # permitted_params
      @user = User.find(params[:id])

      @user.roles.each do |role|
        @user.remove_role role.name
      end

      if params[:user][:password].blank?
        @user.update_without_password(params[:user])
      else
        @user.update_attributes(params[:user])
      end

      if @user.errors.blank?
        redirect_to admin_users_path, :notice => "User updated successfully."
      else
        puts @user.errors.inspect
        render :edit
      end
    end

    def create
      params.permit!
      super
    end
  end
end
