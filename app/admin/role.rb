ActiveAdmin.register Role do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  form do |f|
    f.semantic_errors # shows errors on :base
    f.input :name      # builds an input field for every attribute
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end

  permit_params :name
end