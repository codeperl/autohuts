ActiveAdmin.register Category do
  # Sort categories by left asc
  config.sort_order = 'lft_asc'

  # Add member actions for positioning.
  sortable_tree_member_actions

  index do
    # This adds columns for moving up, down, top and bottom.
    sortable_tree_columns
    column :id
    column :parent_id
    column :name
    column :lft
    column :rgt
    actions
  end

  permit_params :parent_id, :name
end