class Profile < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  belongs_to :user

  ############## Validations ###########################

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  actable

  ################### Scopes ###########################

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end
