class SearchTerm

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :term

  validates_length_of :term, :minimum=>2
end