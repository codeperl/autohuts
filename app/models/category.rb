class Category < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  has_many :products

  ############## Validations ###########################

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  acts_as_nested_set

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || name_changed?
  end

  ################### Scopes ###########################
  default_scope -> {order('lft ASC')}

  scope :find_categories_by_category_id, (lambda do |category_id|
    category_ids = []
    category_ids << category_id
    categories = where(parent_id: category_id)

    categories.each do |category|
      category_ids << category.id
    end

    where(id: category_ids)
  end)

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end