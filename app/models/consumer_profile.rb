class ConsumerProfile < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  has_many :consumer_profile_products, dependent: :destroy

  ############## Validations ###########################

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  acts_as :profile

  ################### Scopes ###########################

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end
