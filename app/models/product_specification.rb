class ProductSpecification < ActiveRecord::Base
  ############## Relationships #########################
  belongs_to :product

  ############## Validations ###########################
  validates_presence_of :name, :value

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################

  ################### Scopes ###########################

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end
