class ProductAttachment < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  belongs_to :product

  ############## Validations ###########################
  validates_presence_of :name, :document

  validates :document, file_size: {less_than_or_equal_to: 4.megabytes,
                                   message: 'document should be less than or equal to %{count}'},
            file_content_type: {allow: ['application/pdf', 'application/msword'], message: 'only %{types} are allowed'} #'application/vnd.ms-excel' for excel files.

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  mount_uploader :document, ProductAttachmentUploader

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || name_changed?
  end
  ################### Scopes ###########################

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end
