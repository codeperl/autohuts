class Product < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  belongs_to :category
  has_one :shop_product, dependent: :destroy
  has_one :consumer_profile_product, dependent: :destroy
  has_many :product_specifications, dependent: :destroy
  has_many :product_pictures, dependent: :destroy
  has_many :product_attachments, dependent: :destroy

  ############## Validations ###########################
  validates_presence_of :name, :description, :price_including_vat, :vat_percent, :stock
  accepts_nested_attributes_for :product_specifications, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :product_pictures, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :product_attachments, :allow_destroy => true, :reject_if => :all_blank

  ############### Callbacks ############################
  before_save :calculate_price_excluding_vat

  ############# Callback methods #######################
  def calculate_price_excluding_vat
    if self.vat_percent > 0
      devide_by = 100 + self.vat_percent
      amount_of_vat = self.price_including_vat * (self.vat_percent/devide_by)
      self.price_excluding_vat = self.price_including_vat - amount_of_vat
    else
      self.price_excluding_vat = self.price_including_vat
    end
  end

  ############# Third-party services ###################
  acts_as_ordered_taggable
  acts_as_commentable
  ratyrate_rateable "quality", "price"

  include PgSearch
  multisearchable :against => [:name, :description, :stock, :price_including_vat]

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || name_changed?
  end

  ################### Scopes ###########################
  scope :lists, -> { includes(:product_pictures).order(created_at: :DESC) }
  scope :products_from_individuals, -> { joins(:consumer_profile_product) }
  scope :shop_products, -> { joins(:shop_product) }
  scope :find_one_by_id, lambda { |id| find(id) }
  scope :find_featured, -> { where('featured = ?', true) }
  scope :recent, -> { order(created_at: :DESC) }
  # { where(created_at: 2.days.ago.beginning_of_day..Date.today.end_of_day).order(created_at: :DESC) }
  ############### Class methods ########################
  def self.get_tags
    ActsAsTaggableOn::Tag.all
  end

  ############### Object methods #######################
  def add_tags
    tags = Product.get_tags

    tags.each do |tag|
      self.tag_list.add(tag.name)
    end
  end

  ############## Protected methods #####################

  ############## Private methods #######################
end