class Role < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true

  ############## Validations ###########################
  validates :resource_type,
            :inclusion => { :in => Rolify.resource_types },
            :allow_nil => true

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  scopify

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || name_changed?
  end
  ################### Scopes ###########################

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end
