class ConsumerProfileProduct < ActiveRecord::Base
  belongs_to :consumer_profile
  belongs_to :product
end
