class User < ActiveRecord::Base
  ############# Attribute accessors ####################
  # Attribute accessor for virtual attribute.
  attr_accessor :login, :role_name
  ############## Relationships #########################
  has_one :profile, dependent: :destroy

  ############## Validations ###########################
  # Validators
  # validates :role_name, role_name: true
  validates_presence_of :username, :password, :password_confirmation
  validate :validate_username

  ############### Callbacks ############################
  # Callbacks
  before_create :create_role_profile

  ############# Callback methods #######################
  def create_role_profile
    add_user_role
    add_profile
  end

  def add_user_role
    if (self.role_name == Roles::CONSUMER) || (self.role_name == Roles::DEALER)
      self.add_role self.role_name
    end
  end

  def add_profile
    if (self.role_name == Roles::CONSUMER)
      @consumer_profile = ConsumerProfile.new
      self.profile = @consumer_profile
    elsif (self.role_name == Roles::DEALER)
      @business_profile = BusinessProfile.new
      self.profile = @business_profile
    end
  end

  ############# Third-party services ###################
  rolify
  ratyrate_rater

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => [:login]

  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || username_changed?
  end
  ################### Scopes ###########################

  ############### Class methods ########################

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_hash).first
    end
  end
=begin

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end
=end

  ############### Object methods #######################

  # https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign-in-using-their-username-or-email-address
  def validate_username
    if User.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  # For active admin I added this.
  # https://groups.google.com/forum/#!topic/activeadmin/2XlAdGJ1HqI
  def password_required?
    new_record? ? false : super
  end

  ############## Protected methods #####################

  ############## Private methods #######################
end
