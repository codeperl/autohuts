class AdminUser < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################

  ############## Validations ###########################

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  ################### Scopes ###########################

  ############### Class methods ########################
=begin
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    where(conditions).where(["lower(email) = :value", { :value => login.strip.downcase }]).first
  end
=end
  ############### Object methods #######################
  def login=(login)
    @login = login
  end

  def login
    @login || self.email
  end
  ############## Protected methods #####################

  ############## Private methods #######################
end
