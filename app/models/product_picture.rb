class ProductPicture < ActiveRecord::Base
  ############# Attribute accessors ####################
  IMAGE_SIZES = {
      :default => [960, 640],
      :thumb => [348, 348],
      :small => [152, 152],
      :logo => [60, 60]
  }
  ############## Relationships #########################
  belongs_to :product

  ############## Validations ###########################
  validates_presence_of :picture, :caption
  validates :picture, file_size: {less_than_or_equal_to: 2.megabytes,
                                  message: 'picture should be less than or equal to %{count}'},
            file_content_type: {allow: ['image/jpg', 'image/jpeg', 'image/png'], message: 'only %{types} are allowed'}

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  mount_uploader :picture, ProductPictureUploader

  extend FriendlyId
  friendly_id :caption, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || caption_changed?
  end
  ################### Scopes ###########################
  scope :cover_image, -> { where(cover_picture: true) }

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end