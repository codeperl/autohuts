class BusinessProfile < ActiveRecord::Base
  ############# Attribute accessors ####################

  ############## Relationships #########################
  has_one :shop, dependent: :destroy
  has_many :business_profile_attachments, foreign_key: "profile_id", dependent: :destroy

  ############## Validations ###########################
  accepts_nested_attributes_for :business_profile_attachments, :allow_destroy => true, :reject_if => :all_blank

  ############### Callbacks ############################

  ############# Callback methods #######################

  ############# Third-party services ###################
  acts_as :profile

  ################### Scopes ###########################

  ############### Class methods ########################

  ############### Object methods #######################

  ############## Protected methods #####################

  ############## Private methods #######################
end