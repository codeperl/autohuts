class Shop < ActiveRecord::Base
  ############# Attribute accessors ####################
  attr_accessor :active_since, :cover_photo_upload_width, :cover_photo_upload_height, :picture_upload_width, :picture_upload_height

  COVER_PHOTO_SIZES = {
      :default => [1352, 358]
  }

  PICTURE_SIZES = {
      :default => [250, 250],
      :thumb => [348, 348],
      :logo => [200, 200]
  }

  ############## Relationships #########################
  has_many :shop_products, dependent: :destroy
  has_many :shop_attachments, dependent: :destroy
  belongs_to :business_profile

  ############## Validations ###########################
  # :sub_domain will be here after configuring this.
  validates_presence_of :name, :description, :address, :mobile, :terms_conditions, :business_profile_id

  #validate :check_cover_photo_dimensions
  #validate :check_picture_dimentions

  accepts_nested_attributes_for :shop_attachments, :allow_destroy => true, :reject_if => :all_blank

  validates :cover_photo, file_size: {less_than_or_equal_to: 4.megabytes,
                                      message: 'cover photo should be less than or equal to %{count}'},
            file_content_type: {allow: ['image/jpg', 'image/jpeg', 'image/png'],
                                message: 'only %{types} are allowed'}

  validates :picture, file_size: {less_than_or_equal_to: 2.megabytes,
                                      message: 'cover photo should be less than or equal to %{count}'},
            file_content_type: {allow: ['image/jpg', 'image/jpeg', 'image/png'],
                                message: 'only %{types} are allowed'}

  ############### Callbacks ############################
  before_create :set_active_since
  after_validation :geocode

  ############# Callback methods #######################
  def set_active_since
    self.active_since = Time.now.to_i
  end

  ############# Third-party services ###################
  acts_as_ordered_taggable_on :business_types

  mount_uploader :cover_photo, ShopCoverPhotoUploader
  mount_uploader :picture, ShopPictureUploader

  include PgSearch
  multisearchable :against => [:name, :description, :sub_domain, :address, :mobile]

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  geocoded_by :address   # can also be an IP address
  def should_generate_new_friendly_id?
    slug.blank? || name_changed?
  end
  ################### Scopes ###########################
  scope :find_featured, -> { where('featured = ?', true) }
  scope :recent, -> { order(created_at: :DESC) }
  # { where(created_at: 2.days.ago.beginning_of_day..Date.today.end_of_day).order(created_at: :DESC) }
  ############### Class methods ########################
  def self.get_business_types
    ActsAsTaggableOn::Tag.all
  end

  ############### Object methods #######################
  def add_business_types
    business_types = Shop.get_business_types

    business_types.each do |business_type|
      self.business_type_list.add(business_type.name)
    end
  end

  # def check_cover_photo_dimensions
  #   ::Rails.logger.info "Cover photo upload dimensions: #{self.cover_photo_upload_width}x#{self.cover_photo_upload_height}"
  #   errors.add :cover_photo, "Dimensions of uploaded cover photo should be not less than 150x150 pixels."
  #   if (
  #   (!cover_photo_cache.nil?) &&
  #     (
  #       (!self.cover_photo_upload_width.nil?) &&
  #       (!self.cover_photo_upload_height.nil?) &&
  #       (self.cover_photo_upload_width < 150 || self.cover_photo_upload_height < 150)
  #     )
  #   )
  #   end
  # end
  #
  # def check_picture_dimentions
  #   ::Rails.logger.info "Picture upload dimensions: #{self.picture_upload_width}x#{self.picture_upload_height}"
  #   errors.add :cover_photo, "Dimensions of uploaded picture should be not less than 150x150 pixels."
  #   if (
  #   (!picture_cache.nil?) &&
  #     (
  #       (!self.picture_upload_width.nil?) &&
  #       (!self.picture_upload_height.nil?) &&
  #       (self.picture_upload_width < 150 || self.picture_upload_height < 150)
  #     )
  #   )
  #   end
  # end

  ############## Protected methods #####################

  ############## Private methods #######################
end