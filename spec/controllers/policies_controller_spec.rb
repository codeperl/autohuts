require 'rails_helper'

RSpec.describe PoliciesController, type: :controller do

  describe "GET #terms_conditions" do
    it "returns http success" do
      get :terms_conditions
      expect(response).to have_http_status(:success)
    end
  end

end
