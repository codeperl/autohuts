require 'rails_helper'

RSpec.describe IndexController, :type => :controller do

  describe "Index controller" do
    it "should be IndexController" do
      expect(controller).to be_an_instance_of(IndexController)
    end

    describe "GET index" do

      it "returns http success" do
        get :index
        expect(response).to have_http_status(:success)
      end

      it "render template" do
        get :index
        expect(response).to render_template(:index)
      end
    end

  end

end
