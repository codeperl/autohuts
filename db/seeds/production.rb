=begin
############################### Global Configurations ###################################
# asset path
asset_path = Rails.root.join('app', 'assets', 'samples').to_s

# image path
image_path = Rails.root.join(asset_path, 'images').to_s

# shops image path
shops_image_path = Rails.root.join(image_path, 'shops').to_s

# shops cover photo path
shops_cover_photo_path = Rails.root.join(shops_image_path, 'cover_photos').to_s

# shops picture path
shops_picture_path = Rails.root.join(shops_image_path, 'pictures').to_s

# individual shop cover photo path
shops_cover_photo_path_a = Rails.root.join(shops_cover_photo_path, 'a.png').to_s
shops_cover_photo_path_b = Rails.root.join(shops_cover_photo_path, 'b.png').to_s

# individual shop image path
shops_picture_path_a = Rails.root.join(shops_picture_path, 'a.png').to_s
shops_picture_path_b = Rails.root.join(shops_picture_path, 'b.jpg').to_s

# product image path
products_image_path = Rails.root.join(image_path, 'products').to_s

# individual product image path
products_image_path_a = Rails.root.join(products_image_path, 'a.png').to_s
products_image_path_b = Rails.root.join(products_image_path, 'b.jpg').to_s
products_image_path_c = Rails.root.join(products_image_path, 'c.jpg').to_s
products_image_path_d = Rails.root.join(products_image_path, 'd.png').to_s
products_image_path_e = Rails.root.join(products_image_path, 'e.jpg').to_s
products_image_path_f = Rails.root.join(products_image_path, 'f.jpg').to_s
products_image_path_g = Rails.root.join(products_image_path, 'g.jpg').to_s
products_image_path_h = Rails.root.join(products_image_path, 'h.jpg').to_s

# attachment path
attachment_path = Rails.root.join(asset_path, 'attachments').to_s

# Business profile attachment
business_profile_attachment_path = Rails.root.join(attachment_path, 'business_profiles').to_s

#individual business profile attachment path
business_profile_attachment_path_a = Rails.root.join(business_profile_attachment_path, 'a.pdf').to_s
business_profile_attachment_path_b = Rails.root.join(business_profile_attachment_path, 'b.pdf').to_s
business_profile_attachment_path_c = Rails.root.join(business_profile_attachment_path, 'c.pdf').to_s
business_profile_attachment_path_d = Rails.root.join(business_profile_attachment_path, 'd.pdf').to_s
business_profile_attachment_path_e = Rails.root.join(business_profile_attachment_path, 'e.pdf').to_s
business_profile_attachment_path_f = Rails.root.join(business_profile_attachment_path, 'f.pdf').to_s
business_profile_attachment_path_g = Rails.root.join(business_profile_attachment_path, 'g.pdf').to_s
business_profile_attachment_path_h = Rails.root.join(business_profile_attachment_path, 'h.pdf').to_s

# Shop attachment
shop_attachment_path = Rails.root.join(attachment_path, 'shops').to_s

#individual shop attachment path
shop_attachment_path_a = Rails.root.join(shop_attachment_path, 'a.pdf').to_s
shop_attachment_path_b = Rails.root.join(shop_attachment_path, 'b.pdf').to_s
shop_attachment_path_c = Rails.root.join(shop_attachment_path, 'c.pdf').to_s
shop_attachment_path_d = Rails.root.join(shop_attachment_path, 'd.pdf').to_s
shop_attachment_path_e = Rails.root.join(shop_attachment_path, 'e.pdf').to_s
shop_attachment_path_f = Rails.root.join(shop_attachment_path, 'f.pdf').to_s
shop_attachment_path_g = Rails.root.join(shop_attachment_path, 'g.pdf').to_s
shop_attachment_path_h = Rails.root.join(shop_attachment_path, 'h.pdf').to_s

# Product attachment
product_attachment_path = Rails.root.join(attachment_path, 'products').to_s

#individual product attachment path
product_attachment_path_a = Rails.root.join(product_attachment_path, 'a.pdf').to_s
product_attachment_path_b = Rails.root.join(product_attachment_path, 'b.pdf').to_s
product_attachment_path_c = Rails.root.join(product_attachment_path, 'c.pdf').to_s
product_attachment_path_d = Rails.root.join(product_attachment_path, 'd.pdf').to_s
product_attachment_path_e = Rails.root.join(product_attachment_path, 'e.pdf').to_s
product_attachment_path_f = Rails.root.join(product_attachment_path, 'f.pdf').to_s
product_attachment_path_g = Rails.root.join(product_attachment_path, 'g.pdf').to_s
product_attachment_path_h = Rails.root.join(product_attachment_path, 'h.pdf').to_s

################################################# Consumer creation #######################################
User.create!(email: 'it.codeperl1@gmail.com', username: 'codeperl1', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl2@gmail.com', username: 'codeperl2', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl3@gmail.com', username: 'codeperl3', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl4@gmail.com', username: 'codeperl4', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl5@gmail.com', username: 'codeperl5', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl6@gmail.com', username: 'codeperl6', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl7@gmail.com', username: 'codeperl7', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)
User.create!(email: 'it.codeperl8@gmail.com', username: 'codeperl8', password: '123456', password_confirmation: '123456', role_name: Roles::DEALER)

################################################# Business creation #######################################
User.create!(email: 'it.codeperl9@gmail.com', username: 'codeperl9', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl10@gmail.com', username: 'codeperl10', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl11@gmail.com', username: 'codeperl11', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl12@gmail.com', username: 'codeperl12', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl13@gmail.com', username: 'codeperl13', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl14@gmail.com', username: 'codeperl14', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl15@gmail.com', username: 'codeperl15', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)
User.create!(email: 'it.codeperl16@gmail.com', username: 'codeperl16', password: '123456', password_confirmation: '123456', role_name: Roles::CONSUMER)

################################################# Non-featured shop creation #######################################
Shop.create!(name: 'Shop #1', sub_domain: 'shop-1', description: 'Shop #1 description', address: 'Shop #1 address', mobile: '01682777240', trade_license: '01', national_id: '3426303407876', featured: false, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_a), picture: File.open(shops_picture_path_a), business_profile_id: 1)
Shop.create!(name: 'Shop #2', sub_domain: 'shop-2', description: 'Shop #2 description', address: 'Shop #2 address', mobile: '01682777241', trade_license: '02', national_id: '3426303407877', featured: false, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_b), picture: File.open(shops_picture_path_b), business_profile_id: 2)
Shop.create!(name: 'Shop #3', sub_domain: 'shop-3', description: 'Shop #1 description', address: 'Shop #3 address', mobile: '01682777242', trade_license: '03', national_id: '3426303407878', featured: false, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_a), picture: File.open(shops_picture_path_a), business_profile_id: 3)
Shop.create!(name: 'Shop #4', sub_domain: 'shop-4', description: 'Shop #2 description', address: 'Shop #4 address', mobile: '01682777243', trade_license: '04', national_id: '3426303407879', featured: false, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_b), picture: File.open(shops_picture_path_b), business_profile_id: 4)

################################################# Featured shop creation #######################################
Shop.create!(name: 'Shop #5', sub_domain: 'shop-5', description: 'Shop #1 description', address: 'Shop #5 address', mobile: '01682777244', trade_license: '05', national_id: '3426303407880', featured: true, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_a), picture: File.open(shops_picture_path_a), business_profile_id: 5)
Shop.create!(name: 'Shop #6', sub_domain: 'shop-6', description: 'Shop #2 description', address: 'Shop #6 address', mobile: '01682777245', trade_license: '06', national_id: '3426303407881', featured: true, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_b), picture: File.open(shops_picture_path_b), business_profile_id: 6)
Shop.create!(name: 'Shop #7', sub_domain: 'shop-7', description: 'Shop #1 description', address: 'Shop #7 address', mobile: '01682777246', trade_license: '07', national_id: '3426303407882', featured: true, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_a), picture: File.open(shops_picture_path_a), business_profile_id: 7)
Shop.create!(name: 'Shop #8', sub_domain: 'shop-8', description: 'Shop #2 description', address: 'Shop #8 address', mobile: '01682777247', trade_license: '08', national_id: '3426303407883', featured: true, terms_conditions: true, cover_photo:  File.open(shops_cover_photo_path_b), picture: File.open(shops_picture_path_b), business_profile_id: 8)

################################################# Shop attachment creation #######################################
ShopAttachment.create!(shop_id: 1, name: 'Shop attachment #1', document: File.open(shop_attachment_path_a))
ShopAttachment.create!(shop_id: 1, name: 'Shop attachment #2', document: File.open(shop_attachment_path_b))

ShopAttachment.create!(shop_id: 2, name: 'Shop attachment #3', document: File.open(shop_attachment_path_c))
ShopAttachment.create!(shop_id: 2, name: 'Shop attachment #4', document: File.open(shop_attachment_path_d))

ShopAttachment.create!(shop_id: 3, name: 'Shop attachment #5', document: File.open(shop_attachment_path_e))
ShopAttachment.create!(shop_id: 3, name: 'Shop attachment #6', document: File.open(shop_attachment_path_f))

ShopAttachment.create!(shop_id: 4, name: 'Shop attachment #7', document: File.open(shop_attachment_path_g))
ShopAttachment.create!(shop_id: 4, name: 'Shop attachment #8', document: File.open(shop_attachment_path_h))

ShopAttachment.create!(shop_id: 5, name: 'Shop attachment #9', document: File.open(shop_attachment_path_a))
ShopAttachment.create!(shop_id: 5, name: 'Shop attachment #10', document: File.open(shop_attachment_path_b))

ShopAttachment.create!(shop_id: 6, name: 'Shop attachment #11', document: File.open(shop_attachment_path_c))
ShopAttachment.create!(shop_id: 6, name: 'Shop attachment #12', document: File.open(shop_attachment_path_d))

ShopAttachment.create!(shop_id: 7, name: 'Shop attachment #13', document: File.open(shop_attachment_path_e))
ShopAttachment.create!(shop_id: 7, name: 'Shop attachment #14', document: File.open(shop_attachment_path_f))

ShopAttachment.create!(shop_id: 8, name: 'Shop attachment #15', document: File.open(shop_attachment_path_g))
ShopAttachment.create!(shop_id: 8, name: 'Shop attachment #16', document: File.open(shop_attachment_path_h))

################################################# Non-featured product creation #######################################
Product.create!(name: 'Product #1', description: 'Product #1 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 1, price_including_vat: 30.00, category_id: 2)
Product.create!(name: 'Product #2', description: 'Product #2 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 2, price_including_vat: 31.00, category_id: 3)
Product.create!(name: 'Product #3', description: 'Product #3 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 3, price_including_vat: 32.00, category_id: 4)
Product.create!(name: 'Product #4', description: 'Product #4 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 4, price_including_vat: 33.00, category_id: 6)

Product.create!(name: 'Product #5', description: 'Product #5 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 5, price_including_vat: 30.00, category_id: 7)
Product.create!(name: 'Product #6', description: 'Product #6 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 6, price_including_vat: 31.00, category_id: 8)
Product.create!(name: 'Product #7', description: 'Product #7 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 7, price_including_vat: 32.00, category_id: 10)
Product.create!(name: 'Product #8', description: 'Product #8 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 8, price_including_vat: 33.00, category_id: 11)

Product.create!(name: 'Product #9', description: 'Product #9 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 9, price_including_vat: 34.00, category_id: 12)
Product.create!(name: 'Product #10', description: 'Product #10 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 10, price_including_vat: 35.00, category_id: 14)
Product.create!(name: 'Product #11', description: 'Product #11 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 11, price_including_vat: 36.00, category_id: 15)
Product.create!(name: 'Product #12', description: 'Product #12 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 12, price_including_vat: 37.00, category_id: 16)

Product.create!(name: 'Product #13', description: 'Product #13 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 13, price_including_vat: 38.00, category_id: 18)
Product.create!(name: 'Product #14', description: 'Product #14 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 14, price_including_vat: 39.00, category_id: 19)
Product.create!(name: 'Product #15', description: 'Product #15 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 15, price_including_vat: 40.00, category_id: 20)
Product.create!(name: 'Product #16', description: 'Product #16 description', featured: false, vat_percent: Vat::PRODUCT_VAT, stock: 16, price_including_vat: 41.00, category_id: 22)

################################################# Featured product creation #######################################
Product.create!(name: 'Product #17', description: 'Product #17 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 17, price_including_vat: 42.00, category_id: 23)
Product.create!(name: 'Product #18', description: 'Product #18 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 18, price_including_vat: 43.00, category_id: 24)
Product.create!(name: 'Product #19', description: 'Product #19 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 19, price_including_vat: 44.00, category_id: 2)
Product.create!(name: 'Product #20', description: 'Product #20 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 20, price_including_vat: 45.00, category_id: 3)

Product.create!(name: 'Product #21', description: 'Product #21 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 21, price_including_vat: 46.00, category_id: 4)
Product.create!(name: 'Product #22', description: 'Product #22 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 22, price_including_vat: 47.00, category_id: 6)
Product.create!(name: 'Product #23', description: 'Product #23 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 23, price_including_vat: 48.00, category_id: 7)
Product.create!(name: 'Product #24', description: 'Product #24 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 24, price_including_vat: 49.00, category_id: 8)

Product.create!(name: 'Product #25', description: 'Product #25 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 25, price_including_vat: 50.00, category_id: 10)
Product.create!(name: 'Product #26', description: 'Product #26 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 26, price_including_vat: 51.00, category_id: 11)
Product.create!(name: 'Product #27', description: 'Product #27 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 27, price_including_vat: 52.00, category_id: 12)
Product.create!(name: 'Product #28', description: 'Product #28 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 28, price_including_vat: 53.00, category_id: 14)

Product.create!(name: 'Product #29', description: 'Product #29 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 29, price_including_vat: 54.00, category_id: 15)
Product.create!(name: 'Product #30', description: 'Product #30 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 30, price_including_vat: 55.00, category_id: 16)
Product.create!(name: 'Product #31', description: 'Product #31 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 31, price_including_vat: 56.00, category_id: 18)
Product.create!(name: 'Product #32', description: 'Product #32 description', featured: true, vat_percent: Vat::PRODUCT_VAT, stock: 32, price_including_vat: 57.00, category_id: 19)

################################################# Shop Product creation #######################################
ShopProduct.create!(shop_id: 1, product_id: 1)
ShopProduct.create!(shop_id: 1, product_id: 2)
ShopProduct.create!(shop_id: 1, product_id: 3)
ShopProduct.create!(shop_id: 1, product_id: 4)

ShopProduct.create!(shop_id: 2, product_id: 5)
ShopProduct.create!(shop_id: 2, product_id: 6)
ShopProduct.create!(shop_id: 2, product_id: 7)
ShopProduct.create!(shop_id: 2, product_id: 8)

ShopProduct.create!(shop_id: 3, product_id: 9)
ShopProduct.create!(shop_id: 3, product_id: 10)
ShopProduct.create!(shop_id: 3, product_id: 11)
ShopProduct.create!(shop_id: 3, product_id: 12)

ShopProduct.create!(shop_id: 4, product_id: 13)
ShopProduct.create!(shop_id: 4, product_id: 14)
ShopProduct.create!(shop_id: 4, product_id: 15)
ShopProduct.create!(shop_id: 4, product_id: 16)

ShopProduct.create!(shop_id: 5, product_id: 17)
ShopProduct.create!(shop_id: 5, product_id: 18)
ShopProduct.create!(shop_id: 5, product_id: 19)
ShopProduct.create!(shop_id: 5, product_id: 20)

ShopProduct.create!(shop_id: 6, product_id: 21)
ShopProduct.create!(shop_id: 6, product_id: 22)
ShopProduct.create!(shop_id: 6, product_id: 23)
ShopProduct.create!(shop_id: 6, product_id: 24)

ShopProduct.create!(shop_id: 7, product_id: 25)
ShopProduct.create!(shop_id: 7, product_id: 26)
ShopProduct.create!(shop_id: 7, product_id: 27)
ShopProduct.create!(shop_id: 7, product_id: 28)

ShopProduct.create!(shop_id: 8, product_id: 29)
ShopProduct.create!(shop_id: 8, product_id: 30)
ShopProduct.create!(shop_id: 8, product_id: 31)
ShopProduct.create!(shop_id: 8, product_id: 32)

################################################# Product picture creation #######################################
ProductPicture.create!(product_id: 1, caption: 'Product picture 1', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 1, caption: 'Product picture 2', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 1, caption: 'Product picture 3', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 1, caption: 'Product picture 4', picture: File.open(products_image_path_d), cover_picture: true)
ProductPicture.create!(product_id: 1, caption: 'Product picture 5', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 1, caption: 'Product picture 6', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 1, caption: 'Product picture 7', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 1, caption: 'Product picture 8', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 2, caption: 'Product picture 9', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 10', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 11', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 12', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 13', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 14', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 15', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 2, caption: 'Product picture 16', picture: File.open(products_image_path_h), cover_picture: true)

ProductPicture.create!(product_id: 3, caption: 'Product picture 17', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 3, caption: 'Product picture 18', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 3, caption: 'Product picture 19', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 3, caption: 'Product picture 20', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 3, caption: 'Product picture 21', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 3, caption: 'Product picture 22', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 3, caption: 'Product picture 23', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 3, caption: 'Product picture 24', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 4, caption: 'Product picture 25', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 4, caption: 'Product picture 26', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 4, caption: 'Product picture 27', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 4, caption: 'Product picture 28', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 4, caption: 'Product picture 29', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 4, caption: 'Product picture 30', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 4, caption: 'Product picture 31', picture: File.open(products_image_path_g), cover_picture: true)
ProductPicture.create!(product_id: 4, caption: 'Product picture 32', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 5, caption: 'Product picture 33', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 5, caption: 'Product picture 34', picture: File.open(products_image_path_b), cover_picture: true)
ProductPicture.create!(product_id: 5, caption: 'Product picture 35', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 5, caption: 'Product picture 36', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 5, caption: 'Product picture 37', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 5, caption: 'Product picture 38', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 5, caption: 'Product picture 39', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 5, caption: 'Product picture 40', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 6, caption: 'Product picture 41', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 42', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 43', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 44', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 45', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 46', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 47', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 6, caption: 'Product picture 48', picture: File.open(products_image_path_h), cover_picture: true)

ProductPicture.create!(product_id: 7, caption: 'Product picture 49', picture: File.open(products_image_path_a), cover_picture: true)
ProductPicture.create!(product_id: 7, caption: 'Product picture 50', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 7, caption: 'Product picture 51', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 7, caption: 'Product picture 52', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 7, caption: 'Product picture 53', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 7, caption: 'Product picture 54', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 7, caption: 'Product picture 55', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 7, caption: 'Product picture 56', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 8, caption: 'Product picture 57', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 8, caption: 'Product picture 58', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 8, caption: 'Product picture 59', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 8, caption: 'Product picture 60', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 8, caption: 'Product picture 61', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 8, caption: 'Product picture 62', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 8, caption: 'Product picture 63', picture: File.open(products_image_path_g), cover_picture: true)
ProductPicture.create!(product_id: 8, caption: 'Product picture 64', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 9, caption: 'Product picture 65', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 9, caption: 'Product picture 66', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 9, caption: 'Product picture 67', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 9, caption: 'Product picture 68', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 9, caption: 'Product picture 69', picture: File.open(products_image_path_e), cover_picture: true)
ProductPicture.create!(product_id: 9, caption: 'Product picture 70', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 9, caption: 'Product picture 71', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 9, caption: 'Product picture 72', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 10, caption: 'Product picture 73', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 10, caption: 'Product picture 74', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 10, caption: 'Product picture 75', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 10, caption: 'Product picture 76', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 10, caption: 'Product picture 77', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 10, caption: 'Product picture 78', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 10, caption: 'Product picture 79', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 10, caption: 'Product picture 80', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 11, caption: 'Product picture 81', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 11, caption: 'Product picture 82', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 11, caption: 'Product picture 83', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 11, caption: 'Product picture 84', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 11, caption: 'Product picture 85', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 11, caption: 'Product picture 86', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 11, caption: 'Product picture 87', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 11, caption: 'Product picture 88', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 12, caption: 'Product picture 89', picture: File.open(products_image_path_a), cover_picture: true)
ProductPicture.create!(product_id: 12, caption: 'Product picture 90', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 12, caption: 'Product picture 91', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 12, caption: 'Product picture 92', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 12, caption: 'Product picture 93', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 12, caption: 'Product picture 94', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 12, caption: 'Product picture 95', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 12, caption: 'Product picture 96', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 13, caption: 'Product picture 97', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 13, caption: 'Product picture 98', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 13, caption: 'Product picture 99', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 13, caption: 'Product picture 100', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 13, caption: 'Product picture 101', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 13, caption: 'Product picture 102', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 13, caption: 'Product picture 103', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 13, caption: 'Product picture 104', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 14, caption: 'Product picture 105', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 14, caption: 'Product picture 106', picture: File.open(products_image_path_b), cover_picture: true)
ProductPicture.create!(product_id: 14, caption: 'Product picture 107', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 14, caption: 'Product picture 108', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 14, caption: 'Product picture 109', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 14, caption: 'Product picture 110', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 14, caption: 'Product picture 111', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 14, caption: 'Product picture 112', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 15, caption: 'Product picture 113', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 15, caption: 'Product picture 114', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 15, caption: 'Product picture 115', picture: File.open(products_image_path_c), cover_picture: true)
ProductPicture.create!(product_id: 15, caption: 'Product picture 116', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 15, caption: 'Product picture 117', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 15, caption: 'Product picture 118', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 15, caption: 'Product picture 119', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 15, caption: 'Product picture 120', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 16, caption: 'Product picture 121', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 16, caption: 'Product picture 122', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 16, caption: 'Product picture 123', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 16, caption: 'Product picture 124', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 16, caption: 'Product picture 125', picture: File.open(products_image_path_e), cover_picture: true)
ProductPicture.create!(product_id: 16, caption: 'Product picture 126', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 16, caption: 'Product picture 127', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 16, caption: 'Product picture 128', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 17, caption: 'Product picture 129', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 17, caption: 'Product picture 130', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 17, caption: 'Product picture 131', picture: File.open(products_image_path_c), cover_picture: true)
ProductPicture.create!(product_id: 17, caption: 'Product picture 132', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 17, caption: 'Product picture 133', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 17, caption: 'Product picture 134', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 17, caption: 'Product picture 135', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 17, caption: 'Product picture 136', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 18, caption: 'Product picture 137', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 18, caption: 'Product picture 138', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 18, caption: 'Product picture 139', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 18, caption: 'Product picture 140', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 18, caption: 'Product picture 141', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 18, caption: 'Product picture 142', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 18, caption: 'Product picture 143', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 18, caption: 'Product picture 144', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 19, caption: 'Product picture 145', picture: File.open(products_image_path_a), cover_picture: true)
ProductPicture.create!(product_id: 19, caption: 'Product picture 146', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 19, caption: 'Product picture 147', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 19, caption: 'Product picture 148', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 19, caption: 'Product picture 149', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 19, caption: 'Product picture 150', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 19, caption: 'Product picture 151', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 19, caption: 'Product picture 152', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 20, caption: 'Product picture 153', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 20, caption: 'Product picture 154', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 20, caption: 'Product picture 155', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 20, caption: 'Product picture 156', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 20, caption: 'Product picture 157', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 20, caption: 'Product picture 158', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 20, caption: 'Product picture 159', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 20, caption: 'Product picture 160', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 21, caption: 'Product picture 161', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 21, caption: 'Product picture 162', picture: File.open(products_image_path_b), cover_picture: true)
ProductPicture.create!(product_id: 21, caption: 'Product picture 163', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 21, caption: 'Product picture 164', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 21, caption: 'Product picture 165', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 21, caption: 'Product picture 166', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 21, caption: 'Product picture 167', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 21, caption: 'Product picture 168', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 22, caption: 'Product picture 169', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 22, caption: 'Product picture 170', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 22, caption: 'Product picture 171', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 22, caption: 'Product picture 172', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 22, caption: 'Product picture 173', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 22, caption: 'Product picture 174', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 22, caption: 'Product picture 175', picture: File.open(products_image_path_g), cover_picture: true)
ProductPicture.create!(product_id: 22, caption: 'Product picture 176', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 23, caption: 'Product picture 177', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 23, caption: 'Product picture 178', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 23, caption: 'Product picture 179', picture: File.open(products_image_path_c), cover_picture: true)
ProductPicture.create!(product_id: 23, caption: 'Product picture 180', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 23, caption: 'Product picture 181', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 23, caption: 'Product picture 182', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 23, caption: 'Product picture 183', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 23, caption: 'Product picture 184', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 24, caption: 'Product picture 185', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 24, caption: 'Product picture 186', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 24, caption: 'Product picture 187', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 24, caption: 'Product picture 188', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 24, caption: 'Product picture 189', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 24, caption: 'Product picture 190', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 24, caption: 'Product picture 191', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 24, caption: 'Product picture 192', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 25, caption: 'Product picture 193', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 194', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 195', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 196', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 197', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 198', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 199', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 25, caption: 'Product picture 200', picture: File.open(products_image_path_h), cover_picture: true)

ProductPicture.create!(product_id: 26, caption: 'Product picture 201', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 26, caption: 'Product picture 202', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 26, caption: 'Product picture 203', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 26, caption: 'Product picture 204', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 26, caption: 'Product picture 205', picture: File.open(products_image_path_e), cover_picture: true)
ProductPicture.create!(product_id: 26, caption: 'Product picture 206', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 26, caption: 'Product picture 207', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 26, caption: 'Product picture 208', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 27, caption: 'Product picture 209', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 27, caption: 'Product picture 210', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 27, caption: 'Product picture 211', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 27, caption: 'Product picture 212', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 27, caption: 'Product picture 213', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 27, caption: 'Product picture 214', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 27, caption: 'Product picture 215', picture: File.open(products_image_path_g), cover_picture: true)
ProductPicture.create!(product_id: 27, caption: 'Product picture 216', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 28, caption: 'Product picture 217', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 28, caption: 'Product picture 218', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 28, caption: 'Product picture 219', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 28, caption: 'Product picture 220', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 28, caption: 'Product picture 221', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 28, caption: 'Product picture 222', picture: File.open(products_image_path_f), cover_picture: true)
ProductPicture.create!(product_id: 28, caption: 'Product picture 223', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 28, caption: 'Product picture 224', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 29, caption: 'Product picture 225', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 29, caption: 'Product picture 226', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 29, caption: 'Product picture 227', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 29, caption: 'Product picture 228', picture: File.open(products_image_path_d), cover_picture: true)
ProductPicture.create!(product_id: 29, caption: 'Product picture 229', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 29, caption: 'Product picture 230', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 29, caption: 'Product picture 231', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 29, caption: 'Product picture 232', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 30, caption: 'Product picture 233', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 30, caption: 'Product picture 234', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 30, caption: 'Product picture 235', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 30, caption: 'Product picture 236', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 30, caption: 'Product picture 237', picture: File.open(products_image_path_e), cover_picture: true)
ProductPicture.create!(product_id: 30, caption: 'Product picture 238', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 30, caption: 'Product picture 239', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 30, caption: 'Product picture 240', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 31, caption: 'Product picture 241', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 31, caption: 'Product picture 242', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 31, caption: 'Product picture 243', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 31, caption: 'Product picture 244', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 31, caption: 'Product picture 245', picture: File.open(products_image_path_e), cover_picture: false)
ProductPicture.create!(product_id: 31, caption: 'Product picture 246', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 31, caption: 'Product picture 247', picture: File.open(products_image_path_g), cover_picture: true)
ProductPicture.create!(product_id: 31, caption: 'Product picture 248', picture: File.open(products_image_path_h), cover_picture: false)

ProductPicture.create!(product_id: 32, caption: 'Product picture 249', picture: File.open(products_image_path_a), cover_picture: false)
ProductPicture.create!(product_id: 32, caption: 'Product picture 250', picture: File.open(products_image_path_b), cover_picture: false)
ProductPicture.create!(product_id: 32, caption: 'Product picture 251', picture: File.open(products_image_path_c), cover_picture: false)
ProductPicture.create!(product_id: 32, caption: 'Product picture 252', picture: File.open(products_image_path_d), cover_picture: false)
ProductPicture.create!(product_id: 32, caption: 'Product picture 253', picture: File.open(products_image_path_e), cover_picture: true)
ProductPicture.create!(product_id: 32, caption: 'Product picture 254', picture: File.open(products_image_path_f), cover_picture: false)
ProductPicture.create!(product_id: 32, caption: 'Product picture 255', picture: File.open(products_image_path_g), cover_picture: false)
ProductPicture.create!(product_id: 32, caption: 'Product picture 256', picture: File.open(products_image_path_h), cover_picture: false)

################################################# Product attachment creation #######################################
ProductAttachment.create!(product_id: 1, name: 'Product attachment 1', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 1, name: 'Product attachment 2', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 2, name: 'Product attachment 3', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 2, name: 'Product attachment 4', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 3, name: 'Product attachment 5', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 3, name: 'Product attachment 6', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 4, name: 'Product attachment 7', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 4, name: 'Product attachment 8', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 5, name: 'Product attachment 9', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 5, name: 'Product attachment 10', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 6, name: 'Product attachment 11', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 6, name: 'Product attachment 12', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 7, name: 'Product attachment 13', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 7, name: 'Product attachment 14', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 8, name: 'Product attachment 15', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 8, name: 'Product attachment 16', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 9, name: 'Product attachment 17', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 9, name: 'Product attachment 18', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 10, name: 'Product attachment 19', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 10, name: 'Product attachment 20', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 11, name: 'Product attachment 21', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 11, name: 'Product attachment 22', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 12, name: 'Product attachment 23', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 12, name: 'Product attachment 24', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 13, name: 'Product attachment 25', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 13, name: 'Product attachment 26', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 14, name: 'Product attachment 27', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 14, name: 'Product attachment 28', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 15, name: 'Product attachment 29', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 15, name: 'Product attachment 30', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 16, name: 'Product attachment 31', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 16, name: 'Product attachment 32', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 17, name: 'Product attachment 33', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 17, name: 'Product attachment 34', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 18, name: 'Product attachment 35', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 18, name: 'Product attachment 36', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 19, name: 'Product attachment 37', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 19, name: 'Product attachment 38', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 20, name: 'Product attachment 39', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 20, name: 'Product attachment 40', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 21, name: 'Product attachment 41', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 21, name: 'Product attachment 42', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 22, name: 'Product attachment 43', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 22, name: 'Product attachment 44', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 23, name: 'Product attachment 45', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 23, name: 'Product attachment 46', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 24, name: 'Product attachment 47', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 24, name: 'Product attachment 48', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 25, name: 'Product attachment 49', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 25, name: 'Product attachment 50', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 26, name: 'Product attachment 51', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 26, name: 'Product attachment 52', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 27, name: 'Product attachment 53', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 27, name: 'Product attachment 54', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 28, name: 'Product attachment 55', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 28, name: 'Product attachment 56', document: File.open(product_attachment_path_h))

ProductAttachment.create!(product_id: 29, name: 'Product attachment 57', document: File.open(product_attachment_path_a))
ProductAttachment.create!(product_id: 29, name: 'Product attachment 58', document: File.open(product_attachment_path_b))

ProductAttachment.create!(product_id: 30, name: 'Product attachment 59', document: File.open(product_attachment_path_c))
ProductAttachment.create!(product_id: 30, name: 'Product attachment 60', document: File.open(product_attachment_path_d))

ProductAttachment.create!(product_id: 31, name: 'Product attachment 61', document: File.open(product_attachment_path_e))
ProductAttachment.create!(product_id: 31, name: 'Product attachment 62', document: File.open(product_attachment_path_f))

ProductAttachment.create!(product_id: 32, name: 'Product attachment 63', document: File.open(product_attachment_path_g))
ProductAttachment.create!(product_id: 32, name: 'Product attachment 64', document: File.open(product_attachment_path_h))

################################################# Product specification creation #######################################
=end
