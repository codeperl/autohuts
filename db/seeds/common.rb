################################################ Admin user creation #####################################
AdminUser.create!(email: 'it.codeperl@gmail.com', password: 'howDoYouDo', password_confirmation: 'howDoYouDo')

############################################### Roles creation ###########################################
Role.create!([{ name: Roles::CONSUMER }, { name: Roles::DEALER }])

############################################### Categories creation ###########################################
cars = Category.create!(name: 'Cars')
Category.create!(:name => 'Parts').move_to_child_of cars
Category.create!(:name => 'Interior').move_to_child_of cars
Category.create!(:name => 'Exterior').move_to_child_of cars
Category.create!(:name => 'Engine Oil & Lubricants').move_to_child_of cars
Category.create!(:name => 'Tyres & Rims').move_to_child_of cars
Category.create!(:name => 'Accessories').move_to_child_of cars
Category.create!(:name => 'Miscellanies').move_to_child_of cars
bikes = Category.create!(name: 'Bikes')
Category.create!(:name => 'Parts').move_to_child_of bikes
Category.create!(:name => 'Engine Oil & Lubricants').move_to_child_of bikes
Category.create!(:name => 'Tyres & Rims').move_to_child_of bikes
Category.create!(:name => 'Accessories').move_to_child_of bikes
Category.create!(:name => 'Miscellanies').move_to_child_of bikes
cycles = Category.create!(name: 'Cycles')
Category.create!(:name => 'Parts').move_to_child_of cycles
Category.create!(:name => 'Tyres & Rims').move_to_child_of cycles
Category.create!(:name => 'Accessories').move_to_child_of cycles
Category.create!(:name => 'Miscellanies').move_to_child_of cycles
commercialVehicles = Category.create!(name: 'Commercial vehicles')
Category.create!(:name => 'Parts').move_to_child_of commercialVehicles
Category.create!(:name => 'Interior').move_to_child_of commercialVehicles
Category.create!(:name => 'Exterior').move_to_child_of commercialVehicles
Category.create!(:name => 'Engine Oil & Lubricants').move_to_child_of commercialVehicles
Category.create!(:name => 'Tyres & Rims').move_to_child_of commercialVehicles
Category.create!(:name => 'Accessories').move_to_child_of commercialVehicles
Category.create!(:name => 'Miscellanies').move_to_child_of commercialVehicles
garage = Category.create!(name: 'Garage')

############################################### Tag creation ###########################################
ActsAsTaggableOn::Tag.create!(name: 'Motor parts')
ActsAsTaggableOn::Tag.create!(name: 'Tyre')
ActsAsTaggableOn::Tag.create!(name: 'Engine Oil & Lubricants')
ActsAsTaggableOn::Tag.create!(name: 'Interior & Exterior')
ActsAsTaggableOn::Tag.create!(name: 'Commercial vehicles')
ActsAsTaggableOn::Tag.create!(name: 'Car')
ActsAsTaggableOn::Tag.create!(name: 'Bike')
ActsAsTaggableOn::Tag.create!(name: 'Cycle')
ActsAsTaggableOn::Tag.create!(name: 'Vulcanize')
ActsAsTaggableOn::Tag.create!(name: 'General')