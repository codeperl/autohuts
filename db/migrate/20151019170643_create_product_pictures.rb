class CreateProductPictures < ActiveRecord::Migration
  def change
    create_table :product_pictures do |t|
      t.belongs_to :product, index: true, foreign_key: true
      t.text :caption
      t.text :picture, null: false
      t.boolean :cover_picture, default: false

      t.timestamps null: false

      # Custom fields
      # Friendly ID
      t.string :slug,           null: false
    end

    # Custom indexes
    # Friendly ID
    add_index :product_pictures, :slug, unique: true
  end
end
