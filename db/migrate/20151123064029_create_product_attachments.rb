class CreateProductAttachments < ActiveRecord::Migration
  def change
    create_table :product_attachments do |t|
      t.belongs_to :product, index: true, foreign_key: true
      t.text :name, null: false
      t.text :document, null: false
      t.timestamps null: false

      # Custom fields
      # Friendly ID
      t.string :slug,           null: false
    end

    # Custom indexes
    # Friendly ID
    add_index :product_attachments, :slug, unique: true
  end
end
