class CreateShopAttachments < ActiveRecord::Migration
  def change
    create_table :shop_attachments do |t|
      t.belongs_to :shop, index: true, foreign_key: true
      t.text :name, null: false
      t.text :document, null: false
      t.timestamps null: false

      # Custom fields
      #F riendly ID
      t.string :slug,           null: false
    end

    # Custom indexes
    # Friendly ID
    add_index :shop_attachments, :slug, unique: true
  end
end
