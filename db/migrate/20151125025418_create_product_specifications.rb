class CreateProductSpecifications < ActiveRecord::Migration
  def change
    create_table :product_specifications do |t|
      t.belongs_to :product, index: true, foreign_key: true
      t.text :name, null: false
      t.text :value, null: false

      t.timestamps null: false
    end
  end
end
