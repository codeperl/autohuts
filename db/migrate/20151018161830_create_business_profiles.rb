class CreateBusinessProfiles < ActiveRecord::Migration
  def change
    create_table :business_profiles do |t|
      t.string :contact_person
      t.string :helpline
      t.text :website
    end
  end
end
