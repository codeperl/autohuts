class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.belongs_to :category, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.boolean :featured, default:false
      t.decimal :vat_percent, precision: 8, scale: 2
      t.decimal :stock, precision: 8, scale: 2
      t.decimal :price_excluding_vat, precision: 8, scale: 2
      t.decimal :price_including_vat, precision: 8, scale: 2
      t.timestamps null: false

      # Custom fields
      # Friendly ID
      t.string :slug,           null: false
    end

    # Custom indexes
    # Friendly ID
    add_index :products, :slug, unique: true
  end
end