class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.string :sub_domain
      t.text :description
      t.text :address
      t.string :mobile
      t.string :national_id
      t.string :trade_license
      t.boolean :featured, default:false
      t.boolean :terms_conditions
      t.text :cover_photo, null: false
      t.text :picture, null: false
      t.timestamps :active_since
      t.timestamps null: false

      # Custom fields
      # Friendly ID
      t.string :slug,           null: false

      #Geo-coder for shops
      t.float :latitude
      t.float :longitude
    end

    # Custom indexes
    # Friendly ID
    add_index :shops, :slug, unique: true
  end
end
