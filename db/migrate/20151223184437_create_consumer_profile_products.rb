class CreateConsumerProfileProducts < ActiveRecord::Migration
  def change
    create_table :consumer_profile_products do |t|
      t.belongs_to :consumer_profile, index: true, foreign_key: true
      t.belongs_to :product, index: true, foreign_key: true
    end
    add_index :consumer_profile_products, [:consumer_profile_id, :product_id], name: 'index_con_prof_prods_on_con_prof_id_and_prod_id'
  end
end
