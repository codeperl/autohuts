class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles, :as_relation_superclass => true do |t|
      t.integer :actable_id
      t.string  :actable_type
      t.belongs_to :user, index: true, foreign_key: true
      t.text :address
      t.string :phone
      t.string :mobile

      t.timestamps null: false
    end
  end
end
