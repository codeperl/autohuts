class ChangeShopsColumnToNullable < ActiveRecord::Migration
  def up
    change_column :shops, :cover_photo, :text, :null => true
    change_column :shops, :picture, :text, :null => true
  end

  def down
    change_column :shops, :cover_photo, :text, :null => false
    change_column :shops, :picture, :text, :null => false
  end
end
