class CreateShopProducts < ActiveRecord::Migration
  def change
    create_table :shop_products do |t|
      t.belongs_to :shop, index: true, foreign_key: true
      t.belongs_to :product, index: true, foreign_key: true
    end
    add_index :shop_products, [:shop_id, :product_id]
  end
end