class CreateConsumerProfiles < ActiveRecord::Migration
  def change
    create_table :consumer_profiles do |t|
      t.string :name
    end
  end
end
