class CreateCategories < ActiveRecord::Migration

  def up
    create_table :categories do |t|
      t.string :name
      t.integer :parent_id, :null => true, :index => true
      t.integer :lft, :null => false, :index => true
      t.integer :rgt, :null => false, :index => true

      # optional fields
      t.integer :depth, :null => false, :default => 0
      t.integer :children_count, :null => false, :default => 0

      # Custom fields
      # Friendly ID
      t.string :slug,           null: false
    end

    # Custom indexes
    # Friendly ID
    add_index :categories, :slug, unique: true
  end

  def down
    drop_table :categories
  end
end