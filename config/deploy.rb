# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'autohuts'
set :repo_url, 'git@bitbucket.org:codeperl/autohuts.git'
set :rbenv_type, :user
set :rbenv_path, '/home/deploy/.rbenv'
set :rbenv_ruby, File.read('.ruby-version').strip
set :deploy_to, '/home/deploy/applications/autohuts/v_1/autohuts'
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
# set :bundle_binstubs, -> { shared_path.join('bin') }            # default: nil
# set :bundle_gemfile, -> { release_path.join('Gemfile') }      # default: nil
# set :bundle_roles, :all
# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      within release_path do
        execute :rake, 'cache:clear'
      end
    end
  end

  desc 'Deploy app for first time'
  task :cold do
    invoke 'deploy:starting'
    invoke 'deploy:started'
    invoke 'deploy:updating'
    invoke 'bundler:install'
    # BACK UP OLD DATABASE
    # if successful DROP OLD DATABASE
    # CREATE NEW DATABASE
    # RUN MIGRATION
    invoke 'deploy:db_schema_manage' # This replaces deploy:migrations
    invoke 'deploy:compile_assets'
    invoke 'deploy:normalize_assets'
    invoke 'deploy:publishing'
    invoke 'deploy:published'
    invoke 'deploy:finishing'
    invoke 'deploy:finished'
  end

  desc 'Setup database'
  task :db_schema_manage do
    on roles(:db) do
      within release_path do
        with rails_env: (fetch(:rails_env) || fetch(:stage)) do
          execute :rake, 'db:schema:load' # This creates the database tables AND seeds
        end
      end
    end
  end

  task :config_symlink do
    on roles(:app) do
      run "cp #{release_path}/config/database.yml #{shared_path}/config/database.yml"
      run "cp #{release_path}/config/secrets.yml #{shared_path}/config/secrets.yml"
    end
  end

  task :create_db do
    on roles(:db) do
      execute "cd #{current_path}; bundle exec rake db:create RAILS_ENV=#{rails_env}"
    end
  end

  task :drop_db do
    on roles(:db) do
      execute "cd #{current_path}; bundle exec rake db:create RAILS_ENV=#{rails_env}"
    end
  end

  # after "deploy", "deploy:create_db"
  # after "deploy", "deploy:migrate"
end