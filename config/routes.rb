Rails.application.routes.draw do
  get 'terms_conditions/index', as: :terms_conditions
  get 'business_profile_attachments/index'
  get 'business_profile_attachments/new'
  get 'business_profile_attachments/edit'
  get 'business_profile_attachments/show'
  get 'business_profile_attachments/destroy'
  get 'policies/terms_conditions', as: :privacy_policy
  get 'search/index', as: :search
  get 'search/result', as: :search_result
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'products/role_products', as: :role_products
  get 'products/products_from_individuals', as: :products_from_individuals
  get 'products/user_products', as: :user_products
  root 'index#index'
  get 'contact/index'
  get 'business_profiles/public_show/:id' => 'business_profiles#public_show', as: :business_profile_public_show
  get 'consumer_profiles/public_show/:id' => 'consumer_profiles#public_show', as: :consumer_profile_public_show
  devise_for :users, controllers: { registrations: "registrations" }
  resources :after_signup
  resources :consumer_profiles
  resources :business_profiles
  resources :shops
  resources :products
  resources :product_pictures
  get 'product_pictures/show_large/:id' => 'product_pictures#show_large', as: :product_picture_show_large
  resources :comments

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
