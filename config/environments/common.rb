# This is the common configuration settings for all environments.

Autohuts::Application.configure do
  config.generators.javascript_engine :js
=begin
  require 'uglifier'
  config.assets.js_compressor = Uglifier.new(output: {ascii_only: true, quote_keys: true})
=end
end